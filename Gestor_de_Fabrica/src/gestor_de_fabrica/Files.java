
package gestor_de_fabrica;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author José Soares
 */
public class Files {
/**
 * Lê do ficheiro máquinas para um objecto da classe Maquina.
 * @param listaMaquinas - objecto da classe Maquina.
 * @throws FileNotFoundException - erros de leitura.
 */
    public static void readTxtToList(Maquina listaMaquinas) throws FileNotFoundException {

        Scanner read = new Scanner(new FileReader("maquinas.txt"));

        ArrayList<String> listaLinhas = new ArrayList<>();

        while (read.hasNextLine()) {
            listaLinhas.add(read.nextLine());
        }

        listaMaquinas.setListaMaquinas(listaLinhas);
    }

    public static void listaParaMatriz(ArrayList<String> listaMaquinas, Maquina matrMaquinas) {
        String[][] matrizMaquinas = new String[listaMaquinas.size()][8];

        for (int i = 0; i < matrizMaquinas.length; i++) {

            matrizMaquinas[i][0] = listaMaquinas.get(i).split(",")[0];
            matrizMaquinas[i][1] = listaMaquinas.get(i).split(",")[1];
            matrizMaquinas[i][2] = listaMaquinas.get(i).split(",")[2];
            matrizMaquinas[i][3] = listaMaquinas.get(i).split(",")[3];
            matrizMaquinas[i][4] = listaMaquinas.get(i).split(",")[4];
            matrizMaquinas[i][5] = listaMaquinas.get(i).split(",")[5];
            matrizMaquinas[i][6] = listaMaquinas.get(i).split(",")[6];
            matrizMaquinas[i][7] = listaMaquinas.get(i).split(",")[7];
        }

        matrMaquinas.setMatrizMaquinas(matrizMaquinas);
    }
}
