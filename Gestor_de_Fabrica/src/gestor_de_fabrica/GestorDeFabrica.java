package gestor_de_fabrica;

import java.io.IOException;

/**
 * 
 * @author José Soares
 */


public class GestorDeFabrica {
    
/**
 * Método main.
 * @param args - parâmetros não utilizados
 * @throws IOException - erros de leitura
 */
    public static void main(String[] args) throws IOException {
        

        
        /////cria um objecto matrizLista 
      
        Maquina matrizLista = new Maquina();
        /////lê os dados do ficheiro para o objecto matriz lista

        Files.readTxtToList(matrizLista); //////em forma de array list
        Files.listaParaMatriz(matrizLista.getListaMaquinas(), matrizLista);//////em forma de matriz
        
        
        /////////para ver conteúdo de lista /////////////
//           for (int i = 0; i < matrizLista.getListaMaquinas().size(); i++) {
//                System.out.println(matrizLista.getListaMaquinas().get(i));
//            }
          

        ///////////////escreve o conteúdo da matriz das máquinas

//        for (int i = 0; i < matrizLista.getMatrizMaquinas().length; i++) {
//            for (int j = 0; j < matrizLista.getMatrizMaquinas()[i].length; j++) {
//                System.out.print(matrizLista.getMatrizMaquinas()[i][j]);
//            }System.out.println("");
//        }


//        /////////////escreve o conteúdo da lista das máquinas


        ///////////////escreve o conteudo da lista das maquinas

//        for (int i = 0; i < matrizLista.getMatrizMaquinas().length; i++) {
//            for (int j = 0; j < matrizLista.getMatrizMaquinas()[i].length; j++) {
//                System.out.print(matrizLista.getMatrizMaquinas()[i][j]);
//            }System.out.println("");
//        }


        //chamar método do menu da classe Menu
        Menu.menu(matrizLista);



    }
}
