package gestor_de_fabrica;

import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author José Soares
 */
public class Menu {

    final static String AUTOR1 = "ALUNO_1";
    final static String AUTOR2 = "ALUNO_2";
    final static String AUTOR3 = "ALUNO_3";
    final static String AUTOR4 = "ALUNO_4";

    /**
     * Método do menu, para fazer escolhas.
     *
     * @param matrizLista - objecto para carregar lista de máquinas.
     * @throws IOException - erros de leitura.
     * @throws FileNotFoundException - erros de leitura.
     */
    public static void menu(Maquina matrizLista) throws IOException, FileNotFoundException {
        Scanner ler = new Scanner(System.in);
        System.out.println("Bem Vindo ao GESTOR DE FABRICA");
        char opcao;
        do {
            //menu do gestor
            System.out.println(" ");
            System.out.println("Menu Principal");
            System.out.println("Escolha uma opção:");
            System.out.println("1- Mostrar as informações de uma determinada máquina");
            opcao = ler.next().charAt(0);
     
            switch (opcao) {
                case '1':////////// Fase 1 //////
                    System.out.println("(1)- " + autor("<4-", AUTOR1, "> ") + "Mostrar as informações de uma determinada máquina.");
                    System.out.println("(2)- <4-ALUNO_2> Alterar capacidade produtiva diária de uma determinada máquina.");
                    System.out.println("(3)- <4-ALUNO_3> Remover determinada máquina da estrutura de dados.");
                    System.out.println("(4)- <4-ALUNO_4> Exportar as informações de uma determinada máquina para ficheiro separado.");
                    System.out.println("(5)- " + autor("<5-", AUTOR1, "> ") + "Ordenar lista em memória por ordem alfabética.");
                    System.out.println("(6)- <5-ALUNO_2> Ordenar lista em memória por ordem decrescente da productividade semanal.");
                    System.out.println("(7)- <5-ALUNO_3> Inserir (por ficheiro) a productividade das máquinas de um determinado dia da semana.");
                    System.out.println("(8)- <5-ALUNO_4> Listar no ecrã toda a informação existente em memória.");
                    System.out.println("(9)- <_Voltar__>");

                    opcao = ler.next().charAt(0);

                    switch (opcao) {

                        case '1'://///////// <4-ALUNO_1>  José Soares //////////////
                            do {
                                System.out.println("(1)- " + autor("<4-", AUTOR1, "> ") + "\n\nInsira um dos códigos abaixo para ver a informação da máquina.\n");
                                Maquina.listaMaqExistentes(matrizLista.getMatrizMaquinas());
                                Maquina verMaquina = new Maquina(Maquina.getLinhaMaquinas(matrizLista));
                                Maquina.infoMaquina(verMaquina);
                            } while (escolha("para ver outra máquina ou"));

                            break;
                        case '2':
                            System.out.println("(2)- <4-ALUNO_2> Alterar capacidade produtiva diária de uma determinada máquina.");
                            break;
                        case '3':
                            System.out.println("(3)- <4-ALUNO_3> Remover determinada máquina da estrutura de dados.");
                            break;
                        case '4':
                            System.out.println("(4)- <4-ALUNO_4> Exportar as informações de uma determinada máquina para ficheiro separado.");
                            break;

                        case '5'://///////// <4-ALUNO_1>  José Soares //////////////

                            Maquina.sortMaquinas(matrizLista);
                            System.out.println("(5)- " + autor("<5-", AUTOR1, "> ") + "\n\nLista de máquinas em memória ordenada por ordem alfabética.\n");

                            while (escolha("para verificação visual")) {
                                for (int i = 0; i < matrizLista.getMatrizMaquinas().length; i++) {
                                    for (int j = 0; j < 8; j++) {
                                        System.out.print(matrizLista.getMatrizMaquinas()[i][j]);
                                    }
                                    System.out.println("");
                                }
                            }

                            break;
                        case '6':
                            System.out.println("(6)- <5-ALUNO_2> Ordenar lista em memória por ordem decrescente da productividade semanal.");
                            break;
                        case '7':
                            System.out.println("(7)- <5-ALUNO_3> Inserir (por ficheiro) a productividade das máquinas de um determinado dia da semana.");
                            break;
                        case '8':
                            System.out.println("(8)- <5-ALUNO_4> Listar no ecrã toda a informação existente em memória.");
                            break;
                        case '9':
                            /////volta atrás/////
                            break;

                        default:
                            System.out.println("input inválido");
                    }
                    break;

                case '2':////////// Fase 2 ///////

                    System.out.println("(1)- " + autor("<7-", AUTOR1, "> ") + "Exportar para ficheiro todas as máquinas com produção semanal inferior a 30% da capacidade.");
                    System.out.println("(2)- <7-ALUNO_2> Inserir productividade de determinada máquina em determinado dia da semana.");
                    System.out.println("(3)- <7-ALUNO_3> Determinar, para cada máquina, os dias da semana em que horas de produção foi maior e menor.");
                    System.out.println("(4)- <7-ALUNO_4> Imprimir num ficheiro de texto a produção semanal.");
                    System.out.println("(5)- " + autor("<8-", AUTOR1, "> ") + "Guardar toda a informação existente em memória num ficheiro(backup).");
                    System.out.println("(6)- <8-ALUNO_2> Recuperar para a memória a informação guardada no ficheiro anterior.");
                    System.out.println("(7)- <8-ALUNO_3> Melhore a eficiência da funcionalidade de <5-ALUNO_3>.");
                    System.out.println("(8)- <8-ALUNO_4> Guardar o caminho onde os ficheiros são guardados.");
                    System.out.println("(9)- <_Voltar__>");

                    opcao = ler.next().charAt(0);

                    switch (opcao) {
                        case '1':
                            System.out.println("(1)- " + autor("<7-", AUTOR1, "> ") + "Exportar para ficheiro todas as máquinas com produção semanal inferior a 30% da capacidade.");
                            break;
                        case '2':
                            System.out.println("(2)- <7-ALUNO_2> Inserir productividade de determinada máquina em determinado dia da semana.");
                            break;
                        case '3':
                            System.out.println("(3)- <7-ALUNO_3> Determinar, para cada máquina, os dias da semana em que horas de produção foi maior e menor.");
                            break;
                        case '4':
                            System.out.println("(4)- <7-ALUNO_4> Imprimir num ficheiro de texto a produção semanal.");
                            break;
                        case '5':
                            System.out.println("(5)- " + autor("<8-", AUTOR1, "> ") + "Guardar toda a informação existente em memória num ficheiro(backup).");
                            break;
                        case '6':
                            System.out.println("(6)- <8-ALUNO_2> Recuperar para a memória a informação guardada no ficheiro anterior.");
                            break;
                        case '7':
                            System.out.println("(7)- <8-ALUNO_3> Melhore a eficiência da funcionalidade de <5-ALUNO_3>.");
                            break;
                        case '8':
                            System.out.println("(8)- <8-ALUNO_4> Guardar o caminho onde os ficheiros são guardados.");
                            break;
                        case '9':
                            /////volta atrás/////
                            break;

                        default:
                            System.out.println("input inválido");
                    }
            }
        } while (opcao != '1');
    }

    /**
     * Método para repetições e para voltar atrás no menu.
     *
     * @param parametro - insere qualquer texto para integrar no contexto.
     * @return - variável boolean s para ser usado na determinação de uma
     * decisão.
     */
    public static boolean escolha(String parametro) {
        System.out.println("\nInsira (s) " + parametro + " (n) para voltar para o menu");
        Scanner sc = new Scanner(System.in);
        boolean s = false;
        boolean repeat = false;
        String input = null;
        do {
            input = sc.next();

            if ("s".equals(input)) {
                s = true;
                repeat = false;

            } else if ("n".equals(input)) {
                s = false;
                repeat = false;
            } else {
                System.out.println("Input incorrecto");
                repeat = true;
            }
        } while (repeat == true);
        return s;
    }

    /**
     * Método para alterar a cor do nome de cada aluno
     *
     * @param antes - string de caracteres antes do nome.
     * @param autor - string com nome do autor (utiliza constantes criadas na
     * classe).
     * @param depois - string de caracteres depois do nome.
     * @return - devolve a junção dos parâmetros com a cor alterado consuante os
     * respectivos nomes(autores).
     */
    public static String autor(String antes, String autor, String depois) {

        String cor1;
        String cor2;

        if (autor == AUTOR1) {
            cor1 = "\033[32;1m";
            cor2 = "\033[32;0m";
        } else if (autor == AUTOR2) {
            cor1 = "\033[31;1m";
            cor2 = "\033[31;0m";

        } else if (autor == AUTOR3) {
            cor1 = "\033[33;1m";
            cor2 = "\033[33;0m";

        } else {
            cor1 = "\033[34;1m";
            cor2 = "\033[34;0m";
        }
        String autorCor = cor1 + antes + autor + depois + cor2;

        return autorCor;
    }

}
