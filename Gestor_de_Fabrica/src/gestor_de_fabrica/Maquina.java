
package gestor_de_fabrica;

import java.util.ArrayList;


import java.util.Arrays;
import java.util.Scanner;


/**
 *
 * @author José Soares
 */
public class Maquina {


    private String[][] matrizMaquinas = new String[200][8];
    ArrayList<String> listaLinhas = new ArrayList();
    
    
    public void setListaMaquinas(ArrayList<String> listaLinhas ){
        this.listaLinhas = listaLinhas;
    }
    
    public ArrayList<String> getListaMaquinas () {
        return listaLinhas;
    }


/**
 * constructor para instância de Maquina sem parâmetros.
 */
    public Maquina() {


    }
    private String nome;
    private int seg;
    private int ter;
    private int qua;
    private int qui;
    private int sex;
    private int sab;
    private int dom;
/**
 * Constructor para instância de Maquina com parâmetros.
 * @param linhaMaquina 
 */
    public Maquina(String[] linhaMaquina) {

        nome = linhaMaquina[0];
        seg = Integer.valueOf(linhaMaquina[1]);
        ter = Integer.valueOf(linhaMaquina[2]);
        qua = Integer.valueOf(linhaMaquina[3]);
        qui = Integer.valueOf(linhaMaquina[4]);
        sex = Integer.valueOf(linhaMaquina[5]);
        sab = Integer.valueOf(linhaMaquina[6]);
        dom = Integer.valueOf(linhaMaquina[7]);
    }
/**
 * Método para buscar o atributo nome, de uma instância de Maquina.
 * @return - para buscar o atributo nome.
 */
    public String getNomeMaq() {
        return this.nome;
    }
/**
 * Método para buscar o atributo seg, de uma instância de Maquina.
 * @return - para buscar o atributo seg.
 */
    public int getSegMaq() {
        return this.seg;
    }
/**
 * Método para buscar o atributo ter, de uma instância de Maquina.
 * @return - para buscar o atributo ter.
 */
    public int getTerMaq() {
        return this.ter;
    }
/**
 * Método para buscar o atributo qua, de uma instância de Maquina.
 * @return - para buscar o atributo qua.
 */
    public int getQuaMaq() {
        return this.qua;
    }
/**
 * Método para buscar o atributo qui, de uma instância de Maquina.
 * @return - para buscar o atributo qui.
 */
    public int getQuiMaq() {
        return this.qui;
    }
/**
 * Método para buscar o atributo sex, de uma instância de Maquina.
 * @return - para buscar o atributo sex.
 */
    public int getSexMaq() {
        return this.sex;
    }
/**
 * Método para buscar o atributo sab, de uma instância de Maquina.
 * @return - para buscar o atributo sab.
 */
    public int getSabMaq() {
        return this.sab;
    }
/**
 * Método para buscar o atributo dom, de uma instância de Maquina.
 * @return - para buscar o atributo dom.
 */
    public int getDomMaq() {
        return this.dom;
    }
/**
 * Método para passar matriz com info do ficheiro para o atributo matrizMaquinas de uma instância de Maquina.
 * @param matrizMaquinas - uma matriz com info do ficheiro.
 */
    public void setMatrizMaquinas(String[][] matrizMaquinas) {
        this.matrizMaquinas = matrizMaquinas;
    }
/**
 * Método para buscar atributo matrizMaquinas, de uma instância de Maquina.
 * @return - para buscar o atributo matrizMaquinas.
 */
    public String[][] getMatrizMaquinas() {
        return this.matrizMaquinas;
    }
/**
 * Método para criar um array com info de apenas uma linha do ficheiro(uma máquina), usando como parâmetro um objecto de Maquina que contém a matriz da informação como atributo.
 * @param matrizMaquina - instancia de objecto de Maquina com matriz da informação como atributo.
 * @return - entrega array com info de apenas uma linha(máquina).
 */
    public static String[] getLinhaMaquinas(Maquina matrizMaquina) { ////usado em <4_ALUNO_1>/////

        Scanner sc = new Scanner(System.in);
        String[][] matrizLista = matrizMaquina.getMatrizMaquinas();
        String[] linhaMaquina = new String[8];
        boolean existeMaq = false;

        do {
            String input = sc.next();

            for (int i = 0; i < matrizLista.length; i++) {

                if (matrizLista[i][0].equalsIgnoreCase(input)) {
                    for (int j = 0; j < matrizLista[i].length; j++) {
                        linhaMaquina[j] = matrizLista[i][j];
                        existeMaq = true;
                    }
                    break;
                }
            }
            if (existeMaq == false) {
                System.out.println("Introduza outro código.");
            }

        } while (existeMaq == false);
        return linhaMaquina;
    }
    /**
     * Método para mostrar os códigos das máquinas em memória, por leitura de uma matriz como parâmetro.
     * @param matrizLista - matriz com info do ficheiro.
     */
     public static void listaMaqExistentes(String [][] matrizLista){
                int counter = 0;
                for (int k = 0; k < matrizLista.length; k++) {
                    System.out.print(matrizLista[k][0] + "  ");
                    counter++;
                    if (counter == 10) {
                        System.out.println("");
                        counter = 0;
                    }
                }
                }
/**
 * Método para escrever a info (os atributos) de uma instância de Maquina.
 * @param verMaquina instância de máquina com atributos (dados de uma linha do ficheiro lido).
 */
    public static void infoMaquina(Maquina verMaquina) { ////usado em <4_ALUNO_1>/////
        System.out.println("\n| Código | Seg | Ter | Qua | Qui | Sex | Sab | Dom |");
        System.out.printf("|%-8s", " " + verMaquina.getNomeMaq());
        System.out.printf("|%-5s", " " + verMaquina.getSegMaq());
        System.out.printf("|%-5s", " " + verMaquina.getTerMaq());
        System.out.printf("|%-5s", " " + verMaquina.getQuaMaq());
        System.out.printf("|%-5s", " " + verMaquina.getQuiMaq());
        System.out.printf("|%-5s", " " + verMaquina.getSexMaq());
        System.out.printf("|%-5s", " " + verMaquina.getSabMaq());
        System.out.println(String.format("|%-5s|", " " + verMaquina.getDomMaq()));
    }

    public static Maquina sortMaquinas(Maquina matrizLista){
        
        String [][] matrizTemp = matrizLista.getMatrizMaquinas();
        String temp=null;
        for (int i = 0; i < matrizTemp.length-1; i++) {
            for (int j = i+1; j < matrizTemp.length; j++) {
                
            
            int sort = matrizTemp[i][0].compareTo(matrizTemp[j][0]);
            if (sort > 0) {
                temp=matrizTemp[i][0];
                matrizTemp[i][0] = matrizTemp[j][0];
                matrizTemp[j][0] = temp;
                for (int k = 1; k < 8; k++) {
                    temp=matrizTemp[i][k];
                    matrizTemp[i][k] = matrizTemp[j][k];
                    matrizTemp[j][k] = temp;
                }

            }
            
        }
        }
        
///        String [] arrayTemp = new String[matrizTemp.length];
//        
//        for (int i = 0; i < matrizTemp.length; i++) {
//            arrayTemp[i] = matrizTemp[i][0];
//        }
        
//        for (int i = 0; i < matrizTemp.length; i++) {
//            Arrays.sort(String.CASE_INSENSITIVE_ORDER.compare(matrizTemp[i], matrizTemp[]));
//           
//        }

//      ArrayList<String> lista = new ArrayList<String>();
//
//for(int i=0;i<matrizTemp.length;i++){
//    for(int j=0;j<matrizTemp[i].length;j++){
//        lista.add(arrayTemp);
//    }}

        matrizLista.setMatrizMaquinas(matrizTemp);
        
//        for (int i = 0; i < lista.size(); i++) {
//            System.out.print(lista.get(i));
//            System.out.println("indice "+ i);
//        }



        
        
        
    return matrizLista;
    }



}
